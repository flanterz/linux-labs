#!/bin/bash
soundfile="alarm.mp3"
DIR="$( cd "$( dirname "$0" )" && pwd )"
#Функция для проигрывания одной мелодии через MPLAYER
play_sound(){
	#убиваем все процессы mplayer-а
	mprocceses=(`pidof mplayer`)
	for proc in ${mprocceses[@]} ; do
		kill $mprocceses
	done

	#включаем MPLAYER
	mplayer -loop 0 -shuffle $soundfile &> /dev/null &
}

pc_sleep(){
  #Отправляем компьютер в сон
  rtcwake -m mem -t `date +%s -d 06:29`
}

off_alarm(){
	#убиваем все процессы mplayer-а
	mprocceses=(`pidof mplayer`)
	for proc in ${mprocceses[@]} ; do
		kill $mprocceses
	done
}

setup_alarm(){
	(crontab -l 2>/dev/null; echo "*/5 * * * * $DIR/alarm.sh<com") | crontab -
	echo "Success!"
}

menu(){
	echo "1) Setup alarm"
	echo "2) Change alarm"
	echo "3) Delete alarm"
	echo "4) Run alarm"
	echo "5) Disable alarm"
	read menuKey
	case $menuKey in
	     	1)
		  echo "Setting up alarm.."
			setup_alarm
		  ;;
	     	2)
		  echo "Changing alarm"
		  ;;
	     	3)
		  echo "Deleting alarm"
		  ;;
				4)
			echo "Playing sound"
			play_sound
			;;
				5)
			off_alarm
			echo "Alarm disabled"
			;;
	     	*)
		  echo "Unknown command"
		  ;;
	esac
}
menu
