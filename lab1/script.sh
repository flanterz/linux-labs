#!/bin/bash
number=1
echo "Введите кол-во папок:"
read n
echo "Задайте шаблон для каждой папки:"
read template
echo "Где создаем папки:"
echo "1)Здесь"
echo "2)Свой путь"
read temp
if [ $temp -eq $number ]
then
  folder=1
else
  read folder
fi
#Создаём папки
for (( i=1; i<=$n; i++ ))
  do
    mkdir -p $folder/$template"_$i"
    #Создаём подпапки
    for (( j=1; j<=$n*2; j++ ))
      do
        mkdir -p $folder/$template"_$i"/"sub_"$template"_$j"
        #Создаём файлы
        for (( d=1; d<=$n*4; d++ ))
          do
            touch $folder/$template"_$i"/"sub_"$template"_$j"/"file_$d"
         done
     done
 done
